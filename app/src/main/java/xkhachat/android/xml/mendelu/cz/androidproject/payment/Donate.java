package xkhachat.android.xml.mendelu.cz.androidproject.payment;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import java.math.BigDecimal;

import xkhachat.android.xml.mendelu.cz.androidproject.R;

public class Donate extends AppCompatActivity {

    PayPalConfiguration m_configuration;
    TextView m_response;
    String m_paypalClientId = "AT3guKFwH4AZeJJKVBmqB_bRSOetv1Ywlb-RaOzCCB0uTx6j0c21y3gSKC5k0vIyaAxtYc6-Qiaez7Fx";
    Intent m_service;
    int m_paypalRequestCode = 999;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.donate_layout);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        m_response = (TextView) findViewById(R.id.response);

        m_configuration = new PayPalConfiguration().environment(PayPalConfiguration.ENVIRONMENT_SANDBOX).clientId(m_paypalClientId);

        m_service = new Intent (this, PayPalService.class);
        m_service.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, m_configuration);
        startService(m_service);
    }

    void pay (View view){
        PayPalPayment payment = new PayPalPayment(new BigDecimal(0.01), "USD", "Test payment with Paypal",
                PayPalPayment.PAYMENT_INTENT_SALE);
        Intent i = new Intent(this,PaymentActivity.class);
        i.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, m_configuration);
        i.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);
        startActivityForResult(i,m_paypalRequestCode);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == m_paypalRequestCode){
            if (resultCode == Activity.RESULT_OK){
                PaymentConfirmation confirmation = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirmation != null){
                    String state = confirmation.getProofOfPayment().getState();
                    if (state == "approved"){
                        AlertDialog.Builder builder = new AlertDialog.Builder(this);
                        builder.setTitle("Payment !");
                        builder.setMessage("Payment was not approved :(");

                        builder.setNegativeButton(" Okay :(",null);

                        AlertDialog dialog = builder.create();
                        dialog.show();

                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(this);
                        builder.setTitle("Payment !");
                        builder.setMessage("Payment was approved :)");

                        builder.setNegativeButton(" Okay :) ",null);

                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                } else {
                    m_response.setText("Confirmation is NULL");
                }
            }
        }
    }
}
